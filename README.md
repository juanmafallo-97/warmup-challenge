# README

### Consideraciones sobre los posts

Los posts a mostrar se mantienen en el estado del componente Home, por eso los datos no son persistentes.

Los posts creados se muestran en la vista del Home al momento de recibir la respuesta exitosa de la api, pero los datos no persisten ya que se actualizan en cada render del componente.

Sucede lo mismo con los eliminados, se remueven de la vista si la petición es exitosa, pero al actualizar la página vuelven a aparecer.

Los posts creados manualmente se muestran de forma correcta, pero claramente las peticiones para ver sus detalles o editarlos darán error ya que su identificador no existe en la base de datos.
