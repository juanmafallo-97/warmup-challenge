import React from "react";
import { Link } from "react-router-dom";
import Navbar from "react-bootstrap/Navbar";
import { Nav, Button } from "react-bootstrap";

const Header = ({ logOut }) => {
  return (
    <Navbar bg="primary" variant="dark" className="p-0 navbar-cont mb-2">
      <Nav className="mr-auto">
        <Link to="/" className="nav-link">
          Home
        </Link>
        <Link to="/edit" className="nav-link">
          Edit Post
        </Link>
        <Button variant="danger" onClick={logOut}>
          Log Out
        </Button>
      </Nav>
    </Navbar>
  );
};

export default Header;
