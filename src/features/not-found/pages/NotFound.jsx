import React from "react";
import { Alert, Container } from "react-bootstrap";

const NotFound = () => {
  return (
    <Container>
      <Alert variant="warning" className="text-center mt-4">
        <h1>Page not found</h1>
        <h3>Error 404</h3>
      </Alert>
    </Container>
  );
};

export default NotFound;
