import React, { useState, useEffect } from "react";
import { Modal, Button } from "react-bootstrap";
import Error from "../../../shared/components/Error";
import fetchData from "../../../shared/services/fetchData";

const DetailsModal = ({ id, show, handleClose }) => {
  const [fetching, setFetching] = useState(false);
  const [post, setPost] = useState(null);
  const [error, setError] = useState(null);

  const getPost = () => fetchData(id, setError, setPost, setFetching);

  useEffect(() => {
    if (id) getPost();
  }, []);

  const renderResult = () => {
    if (fetching) return <p>Loading...</p>;
    if (error) return <Error message={error} />;
    if (post) return post.body;
  };

  return (
    <Modal show={show} onHide={handleClose} className="text-center">
      <Modal.Header className="p-2">
        <Modal.Title className="text-center m-auto">
          {post && post.title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>{renderResult()}</Modal.Body>
      <Modal.Footer className="p-2">
        <Button variant="secondary" onClick={handleClose} className="m-auto">
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default DetailsModal;
