import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Card, Button } from "react-bootstrap";
import "./styles.css";
import DetailsModal from "../DetailsModal";

const PostItem = ({ post, onDelete, setDeleteMessage, setDeleteModalShow }) => {
  const [detailsShow, setDetailsShow] = useState(false);
  const [fetchingDelete, setFetchingDelete] = useState(false);

  const handleDetailsModalClose = () => setDetailsShow(false);

  const deletePost = () => {
    setFetchingDelete(true);
    fetch(`https://jsonplaceholder.typicode.com/posts/${post.id}`, {
      method: "DELETE"
    })
      .then((response) => {
        if (!response.ok)
          throw new Error(
            `An error occurred while deleting post ${post.id}. (Status: ${response.status})`
          );
        setFetchingDelete(false);
        setDeleteMessage(`Post with id ${post.id} successfully deleted`);
        setDeleteModalShow(true);
        onDelete(post.id, null); //sin error
      })
      .catch((error) => {
        setFetchingDelete(false);
        setDeleteMessage(error.message);
        setDeleteModalShow(true);
        onDelete(post.id, true); //con error
      });
  };

  return (
    <div className="card-container">
      <Card className="text-center post-card">
        <Card.Body>
          <Card.Title>{post.title}</Card.Title>
          <div className="d-flex justify-content-around">
            <Button
              variant="success"
              className="m-1"
              onClick={() => setDetailsShow(true)}
            >
              Details
            </Button>
            <Link to={`edit/${post.id}`}>
              <Button variant="primary" className="m-1">
                Edit
              </Button>
            </Link>
            <Button
              variant="danger"
              className="m-1"
              onClick={deletePost}
              disabled={fetchingDelete}
            >
              {fetchingDelete ? "Deleting..." : "Delete"}
            </Button>
          </div>
        </Card.Body>
      </Card>
      {detailsShow && (
        <DetailsModal
          id={post.id}
          show={detailsShow}
          handleClose={handleDetailsModalClose}
        />
      )}
    </div>
  );
};

export default PostItem;
