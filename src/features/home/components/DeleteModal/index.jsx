import React, { useState, useEffect } from "react";
import { Modal, Button, Alert } from "react-bootstrap";

const DeleteModal = ({ message, error, show, handleClose }) => {
  return (
    <Modal show={show} onHide={handleClose} className="text-center">
      <Modal.Body>
        <Alert variant={error ? "danger" : "success"}>{message}</Alert>
      </Modal.Body>
      <Modal.Footer className="p-2">
        <Button variant="secondary" onClick={handleClose} className="m-auto">
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default DeleteModal;
