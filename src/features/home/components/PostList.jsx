import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import Error from "../../shared/components/Error";
import fetchData from "../../shared/services/fetchData";
import PostItem from "./PostItem";
import DeleteModal from "./DeleteModal";

const PostList = ({ posts, error, fetching, setPosts }) => {
  const [deleteMessage, setDeleteMessage] = useState(null);
  const [deleteError, setDeleteError] = useState(null);
  const [deleteModalShow, setDeleteModalShow] = useState(false);

  const setPostIdAndTitle = (posts) => {
    //Esta función es para quedarnos nada más con el id y el título de los posts, para no ocupar memoria innecesariamente ya que es lo único que necesitamos en este componente.
    setPosts(posts.map((post) => ({ id: post.id, title: post.title })));
  };

  const renderPosts = () => {
    if (posts.length)
      return posts.map((post) => (
        <PostItem
          post={post}
          key={post.id}
          onDelete={removePost}
          setDeleteMessage={setDeleteMessage}
          setDeleteModalShow={setDeleteModalShow}
        />
      ));
    else return <p>No posts availabe</p>;
  };

  const removePost = (id, error) => {
    //Sólo se va a remover el post temporalmente, hasta que el componente se vuelva a renderizar y haga nuevamente el fetch al servidor
    if (error) return setDeleteError(true);
    setDeleteError(false);
    setPosts(posts.filter((post) => post.id !== id));
  };

  return (
    <Container>
      <Container className="d-flex flex-wrap justify-content-evenly align-items-center">
        {!fetching ? renderPosts() : <p>Loading posts...</p>}
      </Container>
      {error && <Error message={error} />}
      {deleteModalShow && (
        <DeleteModal
          message={deleteMessage}
          error={deleteError}
          show={deleteModalShow}
          handleClose={() => setDeleteModalShow(false)}
        />
      )}
    </Container>
  );
};

export default PostList;
