import React, { useState, useEffect } from "react";
import { Button, Container } from "react-bootstrap";
import CreateForm from "../../create-post/components/CreateForm";
import PostList from "../components/PostList";

const Home = () => {
  const [error, setError] = useState(null);
  const [fetching, setFetching] = useState(false);
  const [posts, setPosts] = useState([]);
  const [showCreateForm, setShowCreateForm] = useState(false);

  useEffect(() => {
    getPosts();
  }, []);

  const getPosts = () => {
    setFetching(true);
    fetch(` https://jsonplaceholder.typicode.com/posts/
`)
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `An error ocurred fetching posts (Status: ${response.status}. ${
              response.status === 404 ? "Data not found" : ""
            })`
          );
        }
        return response.json();
      })
      .then((posts) => {
        setFetching(false);
        //Solo vamos a guardar los datos que necesitamos mostrar del post. El id y el título
        setPosts(posts.map((post) => ({ id: post.id, title: post.title })));
      })
      .catch((error) => {
        setFetching(false);
        setError(error.message);
      });
  };

  const addCreatedPost = (post) => {
    setPosts([post, ...posts]);
  };

  const cancelPostCreation = () => {
    setShowCreateForm(false);
  };

  return (
    <Container className="text-center">
      <h2 className="m-4">Blog Posts</h2>
      <Button
        onClick={() => setShowCreateForm(true)}
        className={showCreateForm && "d-none"}
      >
        Create new post
      </Button>
      <CreateForm
        show={showCreateForm}
        addCreatedPost={addCreatedPost}
        postsLength={posts.length}
        onCancel={cancelPostCreation}
      />
      <PostList
        posts={posts}
        error={error}
        fetching={fetching}
        setPosts={setPosts}
      />
    </Container>
  );
};

export default Home;
