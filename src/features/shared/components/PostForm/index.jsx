import React from "react";
import { Formik } from "formik";
import { Container, Form, Button } from "react-bootstrap";

const validate = (values) => {
  const errors = {};
  if (!values.title) {
    errors.title = "A title must be provided";
  }
  if (!values.body) {
    errors.body = "The post must contain some content";
  }
  return errors;
};

const PostForm = ({ submitForm, post, onCancel }) => {
  const initialValues = post
    ? {
        title: post.title,
        body: post.body
      }
    : { title: "", body: "" };

  return (
    <Formik
      initialValues={initialValues}
      validate={validate}
      onSubmit={submitForm}
    >
      {(formik) => {
        const {
          values,
          handleSubmit,
          handleChange,
          errors,
          touched,
          handleBlur,
          isValid,
          dirty,
          isSubmitting
        } = formik;
        return (
          <Container className="text-center">
            <Form onSubmit={handleSubmit}>
              <Form.Group>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="title"
                  name="title"
                  id="title"
                  value={values.title}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isValid={touched.title && !errors.title}
                  isInvalid={touched.title && errors.title}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.title}
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group>
                <Form.Label>Content</Form.Label>
                <Form.Control
                  type="body"
                  name="body"
                  id="body"
                  value={values.body}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  isValid={touched.body && !errors.body}
                  isInvalid={touched.body && errors.body}
                />
                <Form.Control.Feedback type="invalid">
                  {errors.body}
                </Form.Control.Feedback>
              </Form.Group>
              <Button
                className="m-2"
                type="submit"
                disabled={!isValid || isSubmitting}
              >
                {isSubmitting ? "Submitting..." : "Submit!"}
              </Button>
              <Button variant="danger" className="m-2" onClick={onCancel}>
                Cancel
              </Button>
            </Form>
          </Container>
        );
      }}
    </Formik>
  );
};

export default PostForm;
