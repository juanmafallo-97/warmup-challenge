import React from "react";
import { Alert } from "react-bootstrap";

const Error = ({ message }) => {
  return (
    <Alert variant="danger" className="text-center m-2">
      {message}
    </Alert>
  );
};

export default Error;
