export default function fetchData(id, setError, setData, setFetching) {
  setFetching(true);
  fetch(` https://jsonplaceholder.typicode.com/posts/${id}
`)
    .then((response) => {
      if (!response.ok) {
        throw new Error(
          `An error ocurred fetching post${
            id ? ` with id ${id}` : "s"
          } (Status: ${response.status}. ${
            response.status === 404 ? "Data not found" : ""
          })`
        );
      }
      return response.json();
    })
    .then((data) => {
      setFetching(false);
      setData(data);
    })
    .catch((error) => {
      setFetching(false);
      setError(error.message);
    });
}
