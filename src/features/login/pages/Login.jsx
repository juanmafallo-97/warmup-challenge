import React, { useState } from "react";
import axios from "axios";
import LoginForm from "../components/LoginForm";
import Error from "../../shared/components/Error";
import { Row, Col } from "react-bootstrap";

const Login = ({ logIn }) => {
  const [error, setError] = useState(null);

  const submitForm = (values, { setSubmitting, resetForm }) => {
    const { password, email } = values;
    axios
      .post("http://challenge-react.alkemy.org/", {
        email: email,
        password: password
      })
      .then((res) => {
        const token = res.data.token;
        setSubmitting(false);
        resetForm();
        logIn(token);
      })
      .catch((error) => {
        console.log(error.response);
        if (error.response) {
          error.response.data.error
            ? setError(error.response.data.error)
            : setError(
                `An error occurred (${error.response.request.status} ${error.response.request.statusText}).`
              );
        } else setError(`An error occurred (${error.message}).`);
        setSubmitting(false);
        resetForm();
      });
  };

  return (
    <Row>
      <Col md={10} lg={8} xl={6} className="m-auto">
        <LoginForm submitForm={submitForm} />
        {error ? <Error message={error} /> : null}
      </Col>
    </Row>
  );
};

export default Login;
