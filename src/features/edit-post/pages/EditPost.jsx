import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { useParams } from "react-router-dom";
import { Container, Alert, Row, Col } from "react-bootstrap";
import PostForm from "../../shared/components/PostForm";
import fetchData from "../../shared/services/fetchData";
import ErrorAlert from "../../shared/components/Error";

const EditPost = () => {
  const { id } = useParams();
  const [fetching, setFetching] = useState(false);
  const [post, setPost] = useState(null);
  const [editSuccessful, setEditSuccessful] = useState(false);
  const [error, setError] = useState(null);

  useEffect(() => {
    if (id) getPost();
  }, []);

  const getPost = () => fetchData(id, setError, setPost, setFetching);

  const submitForm = (values, { setSubmitting }) => {
    const { title, body } = values;
    fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
      method: "PATCH",
      body: JSON.stringify({
        title,
        body
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `An error ocurred editing post (Status: ${response.status})`
          );
        }
        setSubmitting(false);
        return response.json();
      })
      .then((post) => {
        setPost(post);
        console.log(post);
        setEditSuccessful(true);
      })
      .catch((error) => {
        setError(error.message);
        setSubmitting(false);
      });
  };

  const renderMessage = () => {
    return editSuccessful ? (
      <Alert variant="success" className="m-3">
        <div>
          <p>Post with id {id} was successfully edited!</p>
          <p>New title: {post.title}</p>
          <p>New Content: {post.body}</p>
        </div>
      </Alert>
    ) : (
      <ErrorAlert message={error} />
    );
  };

  let history = useHistory();
  const cancelEdit = () => history.push("/");

  return (
    <Container className="text-center">
      <Row className="text-center">
        <Col md={10} lg={8} xl={6} className="m-auto">
          <h1>Edit Post</h1>
          {!id &&
            "No post selected to edit. Please select a post from the list to edit"}
          {fetching && <p>Loading ...</p>}
          {post && (
            <div>
              <h2>Edit your post below</h2>
              <p>You are modifying the post with id: {id}</p>
              <PostForm
                submitForm={submitForm}
                post={post}
                onCancel={cancelEdit}
              />
            </div>
          )}
          {(editSuccessful || error) && renderMessage()}
        </Col>
      </Row>
    </Container>
  );
};

export default EditPost;
