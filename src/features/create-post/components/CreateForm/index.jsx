import React, { useState } from "react";
import { Alert, Container, Row, Col } from "react-bootstrap";
import PostForm from "../../../shared/components/PostForm";
import ErrorAlert from "../../../shared/components/Error";
import "./styles.css";

const CreateForm = ({ show, addCreatedPost, postsLength, onCancel }) => {
  const [error, setError] = useState(null);
  const [post, setPost] = useState(null);
  const [createSuccessful, setCreateSuccessful] = useState(false);

  const submitForm = (values, { setSubmitting }) => {
    const { title, body } = values;
    fetch("https://jsonplaceholder.typicode.com/posts/", {
      method: "POST",
      body: JSON.stringify({
        title,
        body,
        userId: 1
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8"
      }
    })
      .then((response) => {
        if (!response.ok) {
          throw new Error(
            `An error ocurred creating post (Status: ${response.status})`
          );
        }
        setSubmitting(false);
        return response.json();
      })
      .then((newPost) => {
        setPost({ ...newPost, id: postsLength + 1 });
        addCreatedPost({ id: postsLength + 1, title: newPost.title });
        setCreateSuccessful(true);
      })
      .catch((error) => {
        console.log(error);
        setError(error.message);
        setSubmitting(false);
      });
  };

  const renderMessage = () => {
    return createSuccessful ? (
      <Alert variant="success" className="m-3">
        <div>
          <p>Post with id {post.id} was successfully created!</p>
          <p>Post title: {post.title}</p>
          <p>Post content: {post.body}</p>
        </div>
      </Alert>
    ) : (
      <ErrorAlert message={error} />
    );
  };

  return show ? (
    <Container className="text-center">
      <Row className="text-center">
        <Col md={10} lg={8} xl={6} className="m-auto">
          <div className="new-post-container">
            <h2>Create new post</h2>
            <PostForm submitForm={submitForm} onCancel={onCancel} />
            {(createSuccessful || error) && renderMessage()}
          </div>
        </Col>
      </Row>
    </Container>
  ) : null;
};

export default CreateForm;
