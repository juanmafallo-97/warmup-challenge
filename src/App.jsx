import React, { useState } from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import EditForm from "./features/edit-post/pages/EditPost";
import Home from "./features/home/pages/Home";
import Login from "./features/login/pages/Login";
import NotFound from "./features/not-found/pages/NotFound";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./layout/components/Header";

const App = () => {
  const [loggedIn, setLoggedIn] = useState(() =>
    Boolean(localStorage.getItem("blog-token"))
  );

  const logIn = (token) => {
    localStorage.setItem("blog-token", token);
    setLoggedIn(true);
  };
  const logOut = () => {
    localStorage.removeItem("blog-token");
    setLoggedIn(false);
  };

  return (
    <BrowserRouter>
      <Header logOut={logOut} />
      <Switch>
        <Route exact path="/">
          {loggedIn ? <Home /> : <Login logIn={logIn} />}
        </Route>
        <Route exact path="/edit">
          {loggedIn ? <EditForm /> : <Login logIn={logIn} />}
        </Route>
        <Route exact path="/edit/:id">
          {loggedIn ? <EditForm /> : <Login logIn={logIn} />}
        </Route>
        <Route component={NotFound}></Route>
      </Switch>
    </BrowserRouter>
  );
};

export default App;
